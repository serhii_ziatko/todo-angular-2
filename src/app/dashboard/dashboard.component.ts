import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Auth } from '../services/auth.service';
import { Todos } from '../services/todos.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  public items: Array<Item> = null;
  public editItem = null;
  public newItem: Item = {text: ''};
  constructor(
    private router: Router,
    public auth: Auth,
    public todos: Todos,
  ) { }

  ngOnInit() {
    this.todos.getTodos()
      .then(res => {
        this.items = res;
      })
      .catch(err => {
        console.error(err);
      });
  }

  logout() {
    this.auth.logout();
    this.router.navigate(['/auth/signIn']);
  }

  approve(form) {
    if (form.valid && this.editItem.text.trim()) {

      this.todos.editTodo(this.editItem.id, this.editItem)
        .then(res => {
          this.editItem = null;
        })
        .catch(err => {
          console.error(err);
        });
    }
  }

  edit(index) {
    if (confirm('Are you sure? You want to edit this todo!')) {
      this.editItem = this.items[index];
    }
  }

  delete(id, index) {
    if (confirm('Are you sure? You want to delete this todo!')) {
      this.todos.deleteTodo(id)
        .then(() => {
          this.items.splice(index, 1);
        })
        .catch(err => {
          console.error(err);
        });
    }
  }

  add(form) {
    if (form.valid && this.newItem.text.trim()) {
      this.todos.addTodo(this.newItem)
        .then(todo => {
          this.items.push(todo);
          this.newItem = { text: null };
        })
        .catch(err => {
          console.error(err);
        });
    }
  }
}

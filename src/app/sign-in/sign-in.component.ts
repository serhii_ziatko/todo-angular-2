import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormControl, Validators, FormArray, FormBuilder } from '@angular/forms';
import { Auth } from '../services/auth.service';
import { emailPattern } from '../services/validators';

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.css']
})
export class SignInComponent implements OnInit {
 public SignInForm: FormGroup;
  public user: SignInUser = {
    email: '',
    password: '',
  };

  constructor(
    private router: Router,
    public formBuilder: FormBuilder,
    private auth: Auth,
  ) { }

  ngOnInit() {
    this.SignInForm = this.formBuilder.group({
      'email': [this.user.email, [Validators.required, Validators.pattern(emailPattern)]],
      'password': [this.user.password, [Validators.required]],
    });
  }

  signIn() {
    if (this.SignInForm.valid) {
      this.auth.signIn(this.SignInForm.value)
        .then(res => {
          this.auth.setToken(res.token);
          this.auth.setUser(res.user);
          const expiresIn = JSON.stringify(res.expiresIn * 1000 + new Date().getTime());
          this.auth.setStorageVariable('expiresIn', expiresIn);
          this.router.navigate(['']);
        })
        .catch(err => {
          console.log(err);
        });
    }
  }
}

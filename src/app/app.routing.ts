import { Routes } from '@angular/router';
import { DashboardComponent } from './dashboard/dashboard.component';
import { AuthComponent } from './auth/auth.component';
import { SignInComponent } from './sign-in/sign-in.component';
import { SignUpComponent } from './sign-up/sign-up.component';

import { AuthGuard } from './services/authGuard';
import { NoAuthGuard } from './services/noAuthGuard';

export const routes: Routes = [
  {
    path: 'auth',
    pathMatch: 'prefix',
    canActivate: [NoAuthGuard],
    component: AuthComponent,
    children: [
      {
        path: 'signIn',
        component: SignInComponent,
      },
      {
        path: 'signUp',
        component: SignUpComponent,
      },
      { path: '**', redirectTo: 'signIn' }
    ]
  },
  {
    path: '',
    canActivate: [AuthGuard],
    pathMatch: 'prefix',
    component: DashboardComponent,
  },
  { path: '**', redirectTo: '' },
];


export const pages: any = [
  SignInComponent,
  SignUpComponent,
  DashboardComponent,
  AuthComponent,
];

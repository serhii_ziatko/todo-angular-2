import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormControl, Validators, FormArray, FormBuilder } from '@angular/forms';
import { Auth } from '../services/auth.service';
import { emailPattern } from '../services/validators';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.css']
})
export class SignUpComponent implements OnInit {
  public SignUpForm: FormGroup;
  public newUser: NewUser = {
    name: '',
    email: '',
    password: '',
  };

  constructor(
    private router: Router,
    public formBuilder: FormBuilder,
    private auth: Auth,
  ) { }

  ngOnInit() {
    this.SignUpForm = this.formBuilder.group({
      'name': [this.newUser.name, [Validators.required]],
      'email': [this.newUser.email, [Validators.required, Validators.pattern(emailPattern)]],
      'password': [this.newUser.password, [Validators.required]],
    });
  }

  signUp() {
    if (this.SignUpForm.valid) {
      this.auth.signUp(this.SignUpForm.value)
        .then(res => {
          this.auth.setToken(res.token);
          this.auth.setUser(res.user);
          const expiresIn = JSON.stringify(res.expiresIn * 1000 + new Date().getTime());
          this.auth.setStorageVariable('expiresIn', expiresIn);
          this.router.navigate(['']);
        })
        .catch(err => {
          console.log(err);
        });
    }
  }

}

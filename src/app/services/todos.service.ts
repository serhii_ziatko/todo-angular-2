import { Injectable, NgZone } from '@angular/core';
import { AuthHttp } from 'angular2-jwt';
import { Http, Headers } from '@angular/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/map';
import { environment } from '../../environments/environment';

@Injectable()
export class Todos {

  constructor(
    private authHttp: AuthHttp,
    private http: Http,
  ) { }

  getTodos() {
    return this.authHttp.get(`${environment.API}/data/todos`)
      .map(res => res.json())
      .toPromise();
  }

  addTodo(todo) {
    return this.authHttp.post(`${environment.API}/data/todo`, todo)
      .map(res => res.json())
      .toPromise();
  }

  deleteTodo(id) {
    return this.authHttp.delete(`${environment.API}/data/todo/${id}`)
      .toPromise();
  }

  editTodo(id, todo) {
    return this.authHttp.put(`${environment.API}/data/todo/${id}`, todo)
      .map(res => res.json())
      .toPromise();
  }
}

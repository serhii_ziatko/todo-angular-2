import { Injectable, NgZone } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/map';
import { environment } from '../../environments/environment';

@Injectable()
export class Auth {
  public token: string;
  public user: any;

  constructor(
    private http: Http,
  ) {
    this.user = this.getStorageVariable('user');
    this.token = this.getStorageVariable('token');
  }

  public getStorageVariable(name) {
    return JSON.parse(localStorage.getItem(name));
  }

  public setStorageVariable(name, data) {
    localStorage.setItem(name, JSON.stringify(data));
  }

  public setToken(token) {
    this.token = token;
    this.setStorageVariable('token', token);
  }

  public setUser(user) {
    this.user = user;
    this.setStorageVariable('user', user);
  }

  public isAuthenticated() {
    const expiresAt = JSON.parse(localStorage.getItem('expiresIn'));
    return Date.now() < expiresAt;
  }

  public signIn(user) {
    return this.http.post(`${environment.API}/signIn`, user)
      .map(res => res.json())
      .toPromise();
  }

  public signUp(user) {
    return this.http.post(`${environment.API}/signUp`, user)
      .map(res => res.json())
      .toPromise();
  }

  public logout() {
    localStorage.removeItem('token');
    localStorage.removeItem('expiresIn');
    localStorage.removeItem('user');
  }

}

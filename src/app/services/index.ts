import { Auth } from './auth.service';
import { AuthGuard } from './authGuard';
import { NoAuthGuard } from './noAuthGuard';
import { Todos } from './todos.service';

export const services = [
  Auth,
  AuthGuard,
  NoAuthGuard,
  Todos,
];

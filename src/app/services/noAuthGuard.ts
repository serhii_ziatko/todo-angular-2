import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Auth } from './auth.service';


@Injectable()
export class NoAuthGuard implements CanActivate {
  constructor(
    private auth: Auth,
    private router: Router,
  ) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    if (!this.auth.isAuthenticated()) {
      return true;
    };

    this.router.navigate(['']);
    return false;
  }
}

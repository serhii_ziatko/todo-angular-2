/* SystemJS module definition */
declare var module: NodeModule;
interface NodeModule {
  id: string;
}

interface Item {
  id?: string;
  text: string;
  userId?: string;
}

interface NewUser {
  name: string;
  email: string;
  password: string;
}
interface SignInUser {
  email: string;
  password: string;
}
interface User {
  name: string;
  email: string;
}
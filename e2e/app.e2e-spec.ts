import { TestAssignmentPage } from './app.po';

describe('test-assignment App', () => {
  let page: TestAssignmentPage;

  beforeEach(() => {
    page = new TestAssignmentPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});

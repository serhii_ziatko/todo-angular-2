const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const index = require('./router/index.js');
const data = require('./router/data.js');
const jwt = require('jsonwebtoken');
const config = require('config');
const cors = require('cors')

const authMW = (req, res, next) => {
  const token = req.headers['authorization'];
  if (token) {
    jwt.verify(token, config.Auth.secret, (err, decoded) => {
      if (err) {
        res.status(401).send('Invalid token');
      } else {
        const user = {
          id: decoded.id,
          name: decoded.name,
          email: decoded.email,
        }
        req.user = user;
        next();
      }
    })
  } else {
    res.status(401).send('Please sent token');
  }
}

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cors());

app.use('/', index);
app.use('/data', authMW, data);

// catch 404 and forward to error handler
app.use((req, res, next) => {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

app.listen(3000, () => {
  console.log('Example app listening on port 3000!');
});
const express = require('express');
const router = express.Router();
const jwt = require('jsonwebtoken');
const config = require('config');
const users = require('../DBemulator').users


router.post('/signIn', (req, res) => {
  const user = req.body

  const userIndex = users.findIndex(userObj => userObj.email === user.email && userObj.password === user.password);
  if (userIndex < 0) {
    res.status(404).send('User isn\'t exist or password is wrong!')
    return;
  }
  const currUser = users[userIndex]

  const token = jwt.sign(currUser, config.Auth.secret, {
    expiresIn: config.Auth.expiresIn,
  })

  res.json({
    success: true,
    token,
    expiresIn: config.Auth.expiresIn,
    user: {
      id: currUser.id,
      name: currUser.name,
      email: currUser.email,
    },
  });
});

router.post('/signUp', (req, res) => {
  const user = req.body

  const userIndex = users.findIndex(userObj => userObj.email === user.email);
  if (userIndex >= 0) {
    res.status(409).send('Such user already exist!')
    return;
  }
  const newUserId = Date.now();
  const newUser = Object.assign({id: newUserId}, user)
  users.push(newUser);

  const token = jwt.sign(newUser, config.Auth.secret, {
    expiresIn: config.Auth.expiresIn,
  })

  res.json({
    success: true,
    token,
    expiresIn: config.Auth.expiresIn,
    user: {
      id: newUser.id,
      name: newUser.name,
      email: newUser.email,
    },
  });
});

module.exports = router;
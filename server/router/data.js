const express = require('express');
const router = express.Router();
const users = require('../DBemulator').users
const todos = require('../DBemulator').todos

router.get('/todos', (req, res) => {
  const resTodos = todos.filter(item => item.userId === req.user.id);
  res.json(resTodos);
});

router.post('/todo', (req, res) => {
  const newTodo = Object.assign({ userId: req.user.id, id: Date.now() }, req.body)
  todos.push(newTodo);
  res.json(newTodo);
})

router.put('/todo/:id', (req, res) => {
  let editTodo = todos.find(item => item.id === +req.params.id);
  editTodo = Object.assign(editTodo, req.body);
  res.json(editTodo);
})

router.delete('/todo/:id', (req, res) => {
  const deleteTodoIndex = todos.findIndex(item => item.id === +req.params.id);
  if (deleteTodoIndex < 0) {
    res.status(404).send('Todo isn\'t exist');
    return
  };
  todos.splice(deleteTodoIndex, 1);
  res.status(201).send('success');
})

module.exports = router;
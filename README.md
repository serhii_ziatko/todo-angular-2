# TO DO

Hi, I want to help you to run this project. It's a very simple work, please follow the next steps:

    git clone https://serhii_ziatko@bitbucket.org/serhii_ziatko/todo-angular-2.git
    
    cd todo-angular-2
    
    npm install
    
    npm run server
    
open please a new tab in your terminal, move to `todo-angular-2` folder and run

    npm start
    
now you can test this app in a browser, move to `http://localhost:4200/`

Hope all works fine:) Have a good day)